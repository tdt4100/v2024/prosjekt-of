# Eksempelprosjekt for TDT4100 våren 2024: Utgiftskartlegger

## Om repoet
Dette repoet er en "fork" av [prosjekt-base](https://gitlab.stud.idi.ntnu.no/tdt4100/v2024/prosjekt-base). Alle skal ha fått automatisk opprettet et privat repo basert på dette repoet, som inneholder grunnstrukturen for prosjektet dere skal lage selv. Dere kan finne deres eget repo [her](https://gitlab.stud.idi.ntnu.no/tdt4100/v2024/studentprosjekter).

Det finnes løsningsforslag for hva vi gjør i øvingsforelesningene [her](https://gitlab.stud.idi.ntnu.no/tdt4100/v2024/prosjekt-of/-/tree/lf). Dette kan dere allerde se på, og vil være oppdatert med det som er planen å gå gjennom i ØF før selve forelesningen.

## Om prosjektet

Målet er å lage en utgiftskartlegger, som skal hjelpe oss å holde styr på hvor mye penger vi bruker. Dette kan være nyttig for å få oversikt over hvor pengene går, og for å kunne planlegge fremtidige utgifter. Vi ønsker å lagre dataene i en fil, slik at vi kan lagre dataene mellom kjøringer av programmet.

Klassediagrammet under viser hvordan det er tenkt at prosjektet skal implementeres.

![Klassediagram](./classdiagram.png)

## Reminder av nøkkelpunkter

| Nøkkelpunkt                              | Beskrivelse                             |
| ---------------------------------------- | --------------------------------------- |
| Innleveringsfrist                        | 12. april                               |
| Demonstrasjonsfrist hos læringsassistent | 19. april                               |
| Gruppestørrelse                          | 1 eller 2 personer                      |

### Anbefalte perioder å jobbe med prosjektet

| Uke   | Fra  | Til  | Beskrivelse                                 |
| ----- | ---- | ---- | ------------------------------------------- |
| 12    | 18/3 | 24/2 | Grunnklasser og brukergrensesnitt           |
| 13    |      |      | Påske                                       |
| 14    |  1/4 |  7/4 | Lagring of filhåndtering                    |
| 15    |  8/4 | 12/4 | Fullføre appen med tilhørende dokumentasjon |

**_LYKKE TIL_**
